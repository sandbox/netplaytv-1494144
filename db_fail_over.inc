<?php
/**
 * @file
 * Database Fail-over Module
 * 
 * @copyright Copyright (C) 2012 NetPlayTV
 * @author Marcelo Vani <marcelo.vani@netplaytv.com>
 * @author Henri Grech-Cini<henry-grechcini@netplaytv.com>
 * @date    26/02/2012
 *
 */

/*
 * DB Fail-over
 * for the configuration See README.TXT 
 */

require_once './includes/common.inc';
require_once './includes/database.inc';
require_once './includes/file.inc';

global $db_url, $db_fail_over_url;

db_fail_over_set_db_url($db_url, $db_fail_over_url);

/*
 * function db_fail_over_set_db_url
 * Tries to connect to the original db_url, if not sucessful, tries the db_fail_over_url
 */
function db_fail_over_set_db_url(&$db_url, $db_fail_over_url) {
  global $conf;
  
  $original_db_url = $db_url;

  //if sticky is set to true, load previous valid db settings
  //load active db settings
  if ($conf['db_fail_over_sticky'] == TRUE) {
    switch ($conf['db_fail_over_storage']) {
      case 1: //File System
        //load stored information from file system
        $stored_db_url = db_fail_over_load_file(file_directory_path() . '/db_fail_over.txt', $db_url);
        break;
      case 0: //memcache
        default:
        //load stored information from memcache
        $stored_db_url = db_fail_over_load_memcache($db_url);
    }
    //update db_url
    $db_url = $stored_db_url;
  }

  //test db settings
  if (is_array($db_url)) {
    foreach ($db_url as $name => $value) {
      $connect_url = array_key_exists($name, $db_url) ? $db_url[$name] : $db_url['default'];
      if (!db_fail_over_connection_ok($connect_url)) {
         if (!db_fail_over_find_alternative($db_url, $db_fail_over_url, $name)) { //if none of the fail-over dbs worked, switch back to original
           $db_url[$name] = $original_db_url[$name];
         }
      }
    }
  }
  else {
    if (!db_fail_over_connection_ok($db_url)) {
       if (!db_fail_over_find_alternative($db_url, $db_fail_over_url)) { //if none of the fail-over dbs worked, switch back to original
         $db_url = $original_db_url;
       }
    }
  }  
  
  //if sticky is set to true, save current db settings
  if ($conf['db_fail_over_sticky'] == TRUE) {
    if ($stored_db_url != $db_url) {
      switch ($conf['db_fail_over_storage']) {
        case 1: //File System
          db_fail_over_store_file(file_directory_path() . '/db_fail_over.txt', $db_url);
        break;
        case 0: //memcache
        default:
          db_fail_over_store_memcache($db_url);
      }
    }
  }
}

/*
 * function db_fail_over_find_alternative
 * Go throuth fail_over_url array until the connection is successful
 */
function db_fail_over_find_alternative(&$db_url, $db_fail_over_url, $name = 'default') {
  if (is_array($db_fail_over_url[$name])) { //$db_fail_over_url is a multidimensional array
     foreach ($db_fail_over_url[$name] as $key => $value) {
       $connect_url = $value; //$db_fail_over_url[$name][$key];
       $result = db_fail_over_connection_ok($connect_url);
       if ($result) {
          if (is_array($db_url)) {
             $db_url[$name] = $connect_url;
          } 
          else {
            $db_url = $connect_url;
          }
          return TRUE;
       }
     }
  } 
  elseif (is_array($db_fail_over_url)) { //$db_fail_over_url is a monodimensional array
    $connect_url = array_key_exists($name, $db_fail_over_url) ? $db_fail_over_url[$name] : $db_fail_over_url['default'];
    $result = db_fail_over_connection_ok($connect_url);
    if ($result) {
       if (is_array($db_url)) {
          $db_url[$name] = $connect_url;
       } 
       else {
          $db_url = $connect_url;
       }
       return TRUE;
    }
  } 
  elseif (isset($db_fail_over_url)) { //$db_fail_over_url is a string
    $connect_url = $db_fail_over_url;
    $result = db_fail_over_connection_ok($connect_url);
    if ($result) {
       if (is_array($db_url)) {
          $db_url[$name] = $connect_url;
       } 
       else {
          $db_url = $connect_url;
       }
       return TRUE;
    }
  } 
  else { //$db_fail_over_url not set
    return FALSE;
  }
  return FALSE;
}

/*
 * function db_fail_over_connection_ok
 * Tries to connect to the db_url
 */
function db_fail_over_connection_ok($connect_url) {
  $db_type = substr($connect_url, 0, strpos($connect_url, '://'));
  $handler = "./includes/database.$db_type.inc";

  if (is_file($handler)) {
     include_once $handler;
  }
  else {
    _db_error_page("The database type '". $db_type ."' is unsupported. Please use either 'mysql' or 'mysqli' for MySQL, or 'pgsql' for PostgreSQL databases.");
  }

  $connection = db_connect($connect_url);
    
  // db_connect is using array return notation.
  if (is_array($connection)) {
    list($success, $result) = $connection;
    // Connection worked
    if ($success) {
       return TRUE;
    }
    else {
       return FALSE;
    }
  }
  return FALSE;
}

/*
 * function db_fail_over_load_file
 * Load db_url from file
 */
function db_fail_over_load_file($conf_file, $db_url) {
  if (file_exists($conf_file)) {
    $file_contents = file_get_contents($conf_file, TRUE);
    if ($file_contents !== FALSE) {
      $stored_config = unserialize($file_contents);
      if (is_array($stored_config)) {
        $db_url = $stored_config;
      }
    }
  }
  return $db_url;
}

/*
 * function db_fail_over_load_memcache
 * Load db_url from memcache
 */
function db_fail_over_load_memcache($db_url) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_EARLY_PAGE_CACHE);
  $cache = cache_get('db_fail_over_db_url');
  
  if ($cache && !empty($cache->data)) {
    $data = unserialize($cache->data);
    if (is_array($data)) {
      $db_url = $data;
    }
  }
  return $db_url;
}

/*
 * function db_fail_over_store_file
 * Store db_url on file
 */
function db_fail_over_store_file($conf_file, $db_url) {
  $fh = fopen($conf_file, 'w') or drupal_set_message(check_plain(t('Database Fail-over Module error: Can\'t open file ') . $conf_file), 'error');
  //lock the file
  if (flock($fh, LOCK_EX)) {
    //save settings 
    fwrite($fh, serialize($db_url));
    flock($fh, LOCK_UN); // unlock the file
  }
  fclose($fh);
}

/*
 * function db_fail_over_store_memcache
 * Store db_url on memcache
 */
function db_fail_over_store_memcache($db_url) {
  drupal_bootstrap(DRUPAL_BOOTSTRAP_EARLY_PAGE_CACHE);
  cache_set('db_fail_over_db_url', serialize($db_url), 'cache', CACHE_PERMANENT);
}
