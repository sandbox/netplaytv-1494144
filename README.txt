Database Fail-over
------------------------
Requires Drupal 6

Author: Marcelo Vani / Henri Grech-Cini
Copyright: NetplayTv

Overview:
--------
Provides Database fail-over.

Features:
---------
- Automatically switches to any alternative database upon failure of master DB
- A master DB can have multiple fail-over DBs 
- Can be configured to stick with the switched DB for as long as the switched DB is available
- Sticky DB information can be saved on Memcache or File System (On a multi-server setup its advisable the use of distributed Memcache)

Installation:
-------------
- Install the module
- Add the DB fail-over below to your settings.php
   
Configuration:
--------------

Add these lines at the bottom of your settings.php

global $db_fail_over_url;

For an unnamed master connection and a single fail-over database
$db_url = 'mysqli://user:password@master-host/database';
$db_fail_over_url = 'mysqli://user:password@fail-over-host/database';

For an unnamed master connection and multiple fail-over databases
$db_url = 'mysqli://user:password@master-host/database';
$db_fail_over_url = array();
$db_fail_over_url[] = 'mysqli://user:password@fail-over-host-1/database';
$db_fail_over_url[] = 'mysqli://user:password@fail-over-host-2/database';

For a named master connection and a single fail-over database
$db_url = array();
$db_url['default'] = 'mysqli://user:password@master-host/database';
$db_url['another'] = 'mysqli://user:password@another-host/another-database';
$db_fail_over_url = array();
$db_fail_over_url['default'] = 'mysqli://user:password@fail-over-host/database';

For a named master connection and multiple fail-over databases
$db_url = array();
$db_url['default'] = 'mysqli://user:password@master-host/database';
$db_url['another'] = 'mysqli://user:password@another-host/database';
$db_fail_over_url = array();
$db_fail_over_url['default'] = array();
$db_fail_over_url['default'][] 'mysqli://user:password@fail-over-host-1/database';
$db_fail_over_url['default'][] 'mysqli://user:password@fail-over-host-2/database';

$conf['db_fail_over_sticky'] = TRUE; //setting to true will make sure the active DB information will stick  
$conf['db_fail_over_storage'] = 0; //(0)Memcache (1)File System

include_once ('./sites/all/modules/netplaytv/db_fail_over/db_fail_over.inc');



Example: Using multiple Master databases with multiple fail-over databases 

global $db_fail_over_url;

//default connections to host1
$db_url['default']	= 'mysqli://user:pass@host1:3306/default_db';
$db_url['temp']	= 'mysqli://user:pass@host1:3306/temp_db';

//fail-over connections to host2
$db_fail_over_url['default'][]	= 'mysqli://user:pass@host2:3306/default_db';
$db_fail_over_url['temp']	= 'mysqli://user:pass@host2:3306/temp_db';

//fail-over connections to host3
$db_fail_over_url['default'][]	= 'mysqli://user:pass@host3:3306/default_db';

//fail-over connections to host4
$db_fail_over_url['default'][]	= 'mysqli://user:pass@host4:3306/default_db';

//configuration for DB stickiness
$conf['db_fail_over_sticky'] = TRUE; //setting to true will make sure the active DB information will stick

//configuration for storage on File System  
$conf['db_fail_over_storage'] = 1; //(0)Memcache (1)File System

include_once ('./sites/all/modules/netplaytv/db_fail_over/db_fail_over.inc');


Testing
-------
Stopping mysql on the host1 will cause the site to seamlessly switch to host2

Last updated:
------------
; $Id: README.txt,v 1.0 2012/02/26 11:35:58 mvani Exp $
